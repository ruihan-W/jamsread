---  
layout:     post
title:      Yolov1训练自己的数据集
subtitle:         
date:       2020-05-31
author:     JunJun
header-img: img/post-bg-universe.jpg
catalog: true
tags:       
    - 深度学习
    -   
    -   
---

## <center>Yolov1训练自己的数据集        
**环境：**
 系统：ubuntu18.04
 gpu：1050TI
 训练时间：12h，30000次batch
 参考：https://blog.csdn.net/sinat_30071459/article/details/53100791
 ###  一、背景
 我们拥有一个NFPA数据集,里面有282张图片，我想用yolov1实现物体检测
 数据集和标签下载：https://download.csdn.net/download/weixin_43569647/11860219
 ### 二、目录
 
 1. 结果展示
 2. 训练过程及测试
 3. 结果分析
 4. 训练问题及解决方案
 5. 总结
 ###  三、结果展示
**最后31000次效果：**
![在这里插入图片描述](https://img-blog.csdnimg.cn/20191013095918705.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzU2OTY0Nw==,size_16,color_FFFFFF,t_70)![在这里插入图片描述](https://img-blog.csdnimg.cn/20191013100059919.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzU2OTY0Nw==,size_16,color_FFFFFF,t_70)
**我们检测的目标就是这种NFPA704标志：**
![在这里插入图片描述](https://img-blog.csdnimg.cn/20191013100354813.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzU2OTY0Nw==,size_16,color_FFFFFF,t_70)
### 四、训练过程及测试
**1、安装darknet**
对于如何安装darknet我就不多说了，官网有很详细教程，我们需要注意的一点就是，nvcc那一块需要更改，**并且当你每一次都修改了yolo.c中的源码时候，你都需要重新make一遍**
![在这里插入图片描述](https://img-blog.csdnimg.cn/20191013100944638.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzU2OTY0Nw==,size_16,color_FFFFFF,t_70)
**2、数据集准备**
为了更好使用darknet，我们需要将我们的图片准备成为darknet标准格式
目录为：

```python
darknet
	VOCdevkit
		VOC2007
			Annotations (用来存放XML文件，你对图像标记产生的)
			ImageSets
				Main（训练和测试的图片txt文件）
			JPEGImages（图片）
			labels（图片对应的标签，前面第一个数字表示类别，后面的表示）	
			#11 0.34419263456090654 0.611 0.4164305949008499 0.262
			#我的图片大小：360 * 480，它有一个对象，即：dog 
			#image_width = 360 
			#image_height = 480 
			#absolute_x = 30（图片的狗x位置）
			#absolute_y = 40（图片的狗y位置）
			#absolute_height = 200（图片狗的原始高度） ）
			#absolute_width = 200（图片中狗的原始宽度）
			#<class_number>（<absolute_x> / <image_width>）（<absolute_y> / <image_height>）（<absolute_width> / <image_width>）（<absolute_height> / <image_height>）
			#0（30/360）（40 / 480）（200/360）（200/480）
			#0 0.0833 0.0833 0.556 0.417	
```
打标签工具：https://github.com/tzutalin/labelImg
官网有数据处理的过程，我就不多说：https://pjreddie.com/darknet/yolov1/
最后我们数据处理最终基本只需要三个文件：darknet的目录的train.txt文件，labels目录下的，JPEGImages下的，我们到这里数据准备工作也算准备完毕了！
**3、修改代码**
examples/yolo.c

```c
//char *voc_names[] = {"aeroplane", "bicycle", "bird", "boat", "bottle", "bus", "car", "cat", "chair", "cow", "diningtable", "dog", "horse", "motorbike", "person", "pottedplant", "sheep", "sofa", "train", "tvmonitor"};
//你训练的类别名称，分别对应label标签中的第一列，代表数组的第几个类，从0开始
char *voc_names[] = {"NFPA"};

void train_yolo(char *cfgfile, char *weightfile)
{
	//这里修改为你存放train.txt的目录
    char *train_images = "/home/junjun/darknet/train.txt";
    //这里修改为你存放训练权重的目录
    char *backup_directory = "/home/junjun/darknet/backup/";
    srand(time(0));
```

```c
//draw_detections(im, dets, l.side*l.side*l.n, thresh, voc_names, alphabet, 20);
//最后一个参数为训练类别数量
draw_detections(im, dets, l.side*l.side*l.n, thresh, voc_names, alphabet, 1);
```
darknet\cfg\yolov1-tiny.cfg（我这里选择的是训练微小模型）
```python
[net]
# Testing
#batch=1
#subdivisions=1
# Training
batch=64
subdivisions=8
height=448
width=448
channels=3
momentum=0.9
decay=0.0005
```
如果是测试则为：
```python
# Testing
batch=1
subdivisions=1
# Training
#batch=64
#subdivisions=8
height=448
width=448
channels=3
momentum=0.9
decay=0.0005
```
还有下面的
```python
[connected]
output=539  #该值为side*side*（num*5+类别数）
activation=linear

[detection]
classes=1  #修改为你需要训练的种类数
coords=4
rescore=1
side=7
num=2
softmax=0
sqrt=1
jitter=.2
```
主要修改两个地方。也可以修改side和num，side表示网格数，可修改成9*9等；num也可修改，output=side*side*(num*5+classes)
其他的一些参数可以按自己需求修改，比如学习率、max_batches等
**修改完这些东西,接下来请meke一遍，重新编译darknet，我曾经一直报错找不到train.txt**

**4、下载预训练模型**
地址在官网
**5、训练**
```python
sudo ./darknet yolo test cfg/yolov1-tiny.cfg darknet.conv.weights
```
当你看到成功的输出一大堆信息，你就成功了一大半
**6、训练**
```python
执行：sudo ./darknet yolo test cfg/yolov1-tiny.cfg backup/yolov1-tiny_32000.weights
```
然后输入一张测试图片：
![在这里插入图片描述](https://img-blog.csdnimg.cn/20191013193007350.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MzU2OTY0Nw==,size_16,color_FFFFFF,t_70)
### 五、结果分析
在这里我们分别取12000次，22000次，32000次的权重对一些图片进行对比
<figure class="third">
    <img src="https://img-blog.csdnimg.cn/20191013202322611.png" width="300">
    <img src="https://img-blog.csdnimg.cn/20191013203009253.png" width="300">
    <img src="https://img-blog.csdnimg.cn/20191013203244952.png" width="300">
</figure>
<figure class="third">
    <img src="https://img-blog.csdnimg.cn/20191013202413424.png" width="300">
    <img src="https://img-blog.csdnimg.cn/20191013203044823.png" width="300">
    <img src="https://img-blog.csdnimg.cn/2019101320325423.png" width="300">
</figure>

<figure class="third">
    <img src="https://img-blog.csdnimg.cn/20191013202445783.png" width="300">
    <img src="https://img-blog.csdnimg.cn/20191013203125627.png" width="300">
    <img src="https://img-blog.csdnimg.cn/20191013203303701.png" width="300">
</figure>

<figure class="third">
    <img src="https://img-blog.csdnimg.cn/20191013203503373.png" width="300">
    <img src="https://img-blog.csdnimg.cn/20191013203154434.png" width="300">
    <img src="https://img-blog.csdnimg.cn/20191013203311605.png" width="300">
</figure>

<figure class="third">
    <img src="https://img-blog.csdnimg.cn/20191013203511789.png" width="300">
    <img src="https://img-blog.csdnimg.cn/2019101320320338.png" width="300">
    <img src="https://img-blog.csdnimg.cn/20191013203318561.png" width="300">
</figure>

<figure class="third">
    <img src="https://img-blog.csdnimg.cn/20191013203836335.png" width="300">
    <img src="https://img-blog.csdnimg.cn/20191013203857825.png" width="300">
    <img src="https://img-blog.csdnimg.cn/20191013203916804.png" width="300">
</figure>

我们可以看出，随着迭代次数的增加，框也逐渐准确，分类的概率也逐渐增大
### 六、训练问题及解决方案

 **1. 训练一晚上后，第二天去登陆ubuntu系统，输入密码后，系统又自动调回的登陆界面**
我重新卸载了nvdia驱动，重新安装也没用，网上的一些方法也尝试了都不管用，原因，空间不够，删掉一些东西，就可重新进入
 **2. 出现batch=1，-nan情况**
 注释掉Testing的部分，使用Training部分
 **3.数据集训练完成，但是检测没有任何框，出现**
 训练过程出现问题，数据集和模型存在问题，重新检查数据集和模型的正确性
### 七、总结
对于训练自己的数据集，我觉得其中最重要的一步就是准备好自己的数据集，将数据集按照voc的格式准备好，训练的速度和单一类图片的数量无关，和图片的像素大小有训练的种类数有关，yolo的训练速度实在是慢，曾经训练voc数据集，训练了三天，仅仅迭代20000次不到。
